/*******************************************************************************
* Project Name: Switch2Smart
*
* Version: 1.0
*
* Description:
* In this project ESP8266 controls AC input
* Owner:
* silicosmos
*
********************************************************************************
* Copyright (2020-21) , silicosmos.in
*******************************************************************************/

#include <Arduino.h>
#include <ESP8266Wifi.h>
#include <TaskScheduler.h>
#include <ESPUtils.h>
#include <GPIOV1.h>
#include <WifiControlV2.h>
#include <ESPHttpClient.h>
#include <FS.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>
#include <ESPMemory.h>
#include <ESPAlexaV2.h>
#include "ESP8266FtpServer.h"
#include <ACControl.h>
#include <interrupts.h>
#include <ESPFile.h>
#include <Application.h>
#include <AsyncMqttClient.h>

const char* htmlfile = "/index.html";
IPAddress local_IP(192,168,4,1);
IPAddress gateway(192,168,4,0);
IPAddress subnet(255,255,255,0);

ESP8266WebServer ESPServer(80);

/* ESP Server functions*/
void ESPServer_HandleWebRequests(void);
void ESPServer_HandleValues(void);
void ESPServer_HandleRoot(void);
bool ESPServer_LoadFromSpiffs(String path);
String CreateJsonResponse(void);
void ESP_Handle_Wifi_Add(void);
void ESP_Handle_Wifi_Hostname(void);
void ESP_Handle_Wifi_Parameter_Add(void);
void ESP_Handle_Alexa_Parameter(void);
void ESP_Handle_Mqtt_Parameter(void);
void ESP_Handle_Device_Parameter(void);
void ESP_Handle_DeviceReset_Parameter(void);
void ESP_Handle_Alexa_Values(void);
void ESP_Handle_AC_Values(void);
void ESP_Handle_AC_Websetvalues(void);
void ESP_Handle_AC_Testvalues(void);

void Application_Init(void);

// Callback methods prototypes
void TestCallback(void);
void WifiControlV1Callback(void);
void InternetCallback(void);
void MqttCallback(void);
void Mqtt_Check_For_Alexa_Sync(void);

void Timer_100us_Start(void);

void ICACHE_RAM_ATTR timercallback100us(void) ;
void ICACHE_RAM_ATTR  ZeroCrossingCallback(void) ;


void onMqttConnect(bool sessionPresent);
void onMqttDisconnect(AsyncMqttClientDisconnectReason reason);
void onMqttSubscribe(uint16_t packetId, uint8_t qos);
void onMqttUnsubscribe(uint16_t packetId);
void onMqttMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t                          len, size_t index, size_t total);
void onMqttPublish(uint16_t packetId);
void connectToMqtt(void);
void MQTT_Handle(void);
void Mqtt_Publish_All(void);

Task TestTask(1000, TASK_FOREVER, &TestCallback);
Task WifiControlTask(1000, TASK_FOREVER, &WifiControlV1Callback);
//Task InternetTask(60000, TASK_FOREVER, &InternetCallback);
Task MqttControlTask(60000, TASK_FOREVER, &MqttCallback);
//Task MqttControlTask(10000, TASK_FOREVER, &MqttCallback);
Task MqttReconnectTask(2000,TASK_ONCE,&connectToMqtt);

Scheduler TaskRunner;

FtpServer ftpSrv;

uint8_t ui8Test = 234;
String sDeviceRestartAt;
uint16_t ui16ZeroCrossCounter;
uint8_t ui8DeviceResetFlag=0;

long lRSSI;
FSInfo fs_info;
IPAddress IPAddr;

// MQTT 
AsyncMqttClient mqttClient;
uint8_t ui8MqttSendFlag;


WiFiEventHandler wifiConnectHandler;
WiFiEventHandler wifiDisconnectHandler;

GPIOV1 WiFiLed;

void setup(){
    Application_Init();
    ftpSrv.begin("esp8266", "esp8266");
    mqttClient.onConnect(onMqttConnect);
    mqttClient.onDisconnect(onMqttDisconnect);
    mqttClient.onSubscribe(onMqttSubscribe);
    mqttClient.onUnsubscribe(onMqttUnsubscribe);
    mqttClient.onMessage(onMqttMessage);
    mqttClient.onPublish(onMqttPublish);
    connectToMqtt();
}

void loop(){
    TaskRunner.execute();
    ESPServer.handleClient();
    MAlexa.handleRequest();
    ftpSrv.handleFTP();
    if(ui8DeviceResetFlag){
        delay(2000);
        ESP.restart();
    }
    yield();
}

void Application_Init(void){
    delay(5000);
    
    //WiFiLed.Start(D6,OUTPUT,0);
    //WiFiLed.Off();

    Serial1.begin(921600);
    //Serial1.setDebugOutput(true);
    Serial1.begin(921600);
    //Serial1.setDebugOutput(true);
    Serial1.setDebugOutput(false);

    Serial1.println();
    Serial1.println("ESPACControlV100");
    EMemory.Start();
    
     //Initialize File System
    if(SPIFFS.begin()){
        EUtils.println("File System Initialized sucess ");
    }else{
        EUtils.println("File System Initialized failed ");
    }

    SPIFFS.info(fs_info);
    EUtils.println("File Info");    
    EUtils.println(fs_info.totalBytes);
    EUtils.println(fs_info.usedBytes);
    EUtils.println(fs_info.blockSize);
    EUtils.println(fs_info.pageSize);
    EUtils.println(fs_info.maxOpenFiles);
    EUtils.println(fs_info.maxPathLength);
    
    EFile.Start();

    Serial1.println("Connecting to wifi");

    EWifiControl.Start();
    IPAddr = WiFi.localIP();
    Serial1.println(IPAddr);
    

    EUtils.Start();
    EUtils.Enable_Serial_Debug();
    //EUtils.Disable_Serial1_Debug();

    String realSize = String(ESP.getFlashChipRealSize());
    String ideSize = String(ESP.getFlashChipSize());
    bool flashCorrectlyConfigured = realSize.equals(ideSize);

    EUtils.println(realSize);
    EUtils.println(ideSize);
    EUtils.println(flashCorrectlyConfigured);

   

    //EUtils.println("File System formating started");
    //SPIFFS.format();
    //EUtils.println("File System formating completed");

    /* Enable ESP Server */
    //Initialize Webserver
    ESPServer.on("/",ESPServer_HandleRoot);
    
    ESPServer.on("/getwifivalues",ESPServer_HandleValues);
    ESPServer.on ("/getdevicevalues", ESP_Handle_Alexa_Values);
    ESPServer.on ("/getacvalues", ESP_Handle_AC_Values);
    

    ESPServer.on ("/addwifi", ESP_Handle_Wifi_Add);
    ESPServer.on ("/addwifihostname", ESP_Handle_Wifi_Hostname);
    ESPServer.on ("/addwifiparameter", ESP_Handle_Wifi_Parameter_Add);
    ESPServer.on ("/addalexa", ESP_Handle_Alexa_Parameter);
    ESPServer.on ("/adddevice", ESP_Handle_Device_Parameter);
    ESPServer.on ("/addmqtt", ESP_Handle_Mqtt_Parameter);
    ESPServer.on ("/reset", ESP_Handle_DeviceReset_Parameter);
    ESPServer.on ("/setacvalue", ESP_Handle_AC_Websetvalues);
    ESPServer.on ("/settestvalue", ESP_Handle_AC_Testvalues);

    ESPServer.onNotFound(ESPServer_HandleWebRequests);
    
    
    ESPServer.begin();
    EUtils.print("Setting soft-AP configuration ... ");
    EUtils.println(WiFi.softAPConfig(local_IP, gateway, subnet) ? "Ready" : "Failed!");
    EUtils.print("Setting soft-AP ... ");
    String sAPName;
    String sAPPassword;
    if(EWifiControl.sHostName != "NOWIFI"){
        sAPName = "SS01" + EWifiControl.sHostName;
    }else{
        sAPName = "SS01" + EUtils.Get_MAC();
    }

    sAPPassword = EWifiControl.sHostPassword;
    EUtils.print("APName: ");
    EUtils.print(sAPName);
    EUtils.print("  APPassword: ");
    EUtils.print(sAPPassword);
    EUtils.print("  Status: ");
    EUtils.println(WiFi.softAP(sAPName,sAPPassword) ? "Ready" : "Failed!");
    EUtils.print("Soft-AP IP address = ");
    EUtils.println(WiFi.softAPIP());

    TaskRunner.init();
    EUtils.println("Initialized scheduler");

    TaskRunner.addTask(TestTask);
    EUtils.println("added TestTask");
    TestTask.enable();
    EUtils.println("Enabled TestTask");

    TaskRunner.addTask(WifiControlTask);
    EUtils.println("added WifiControlTask");
    WifiControlTask.enable();
    EUtils.println("Enabled WifiControlTask");

    //TaskRunner.addTask(InternetTask);
    //EUtils.println("added InternetTask");
    //InternetTask.enable();
    //EUtils.println("Enabled InternetTask");

    TaskRunner.addTask(MqttControlTask);
    EUtils.println("added MqttControlTask");
    MqttControlTask.enable();
    EUtils.println("Enabled MqttControlTask");

    TaskRunner.addTask(MqttReconnectTask);

    MAlexa.Start();

    sDeviceRestartAt = EUtils.GetTime();

    noInterrupts();
    Timer_100us_Start();
    pinMode(ZERO_CROSS_PIN,INPUT);
    attachInterrupt(digitalPinToInterrupt(ZERO_CROSS_PIN),ZeroCrossingCallback,FALLING);
    interrupts();
    
    AC.Start();
}

void TestCallback(void) {
    //Serial1.println("Working");
}   

void WifiControlV1Callback(void){
    String sString;
    
    //EUtils.print("INT:");
    //MRTC.RTC_Read();
    //EUtils.print("RTC:");
    //EUtils.println(MRTC.RTC_Print());
    Mqtt_Check_For_Alexa_Sync();
    sString = EWifiControl.Checkout_Connection();
    if(EWifiControl.Get_Status() != WL_CONNECTED){
        // show error indication
       // WiFiLed.Toggle();
       Serial1.println("no wifi");

    }else{
        Serial1.println("testing");
       // WiFiLed.Off();
        //EHttpClient.Check_Google_Server(500);
        //EHttpClient.Check_ESP_Default_Ping(500);
    }
}

void InternetCallback(void){
    EHttpClient.Check_ESP_Default_Ping(1000);
}

void MqttCallback (void){
    MQTT_Handle();
}

void Timer_100us_Start(void){
    timer1_isr_init();
    timer1_attachInterrupt(timercallback100us);
    timer1_enable(TIM_DIV1, TIM_EDGE, TIM_LOOP); //80MHZ
    timer1_write(8000);//100us
}

void ICACHE_RAM_ATTR timercallback100us(void){
    //noInterrupts();
    //AC.Check_Timer_10ms();
    AC.Check_Timer_20ms();
    //interrupts();   
}

void ICACHE_RAM_ATTR ZeroCrossingCallback(void){
    //noInterrupts();
    AC.ZeroCrossDetect();
    //interrupts();
}

bool ESPServer_LoadFromSpiffs(String path){

    String dataType = "text/plain";
    if(path.endsWith("/")) path += "index.htm";
    if(path.endsWith(".src")) path = path.substring(0, path.lastIndexOf("."));
    else if(path.endsWith(".html")) dataType = "text/html";
    else if(path.endsWith(".htm")) dataType = "text/html";
    else if(path.endsWith(".css")) dataType = "text/css";
    else if(path.endsWith(".js")) dataType = "application/javascript";
    else if(path.endsWith(".png")) dataType = "image/png";
    else if(path.endsWith(".gif")) dataType = "image/gif";
    else if(path.endsWith(".jpg")) dataType = "image/jpeg";
    else if(path.endsWith(".ico")) dataType = "image/x-icon";
    else if(path.endsWith(".xml")) dataType = "text/xml";
    else if(path.endsWith(".pdf")) dataType = "application/pdf";
    else if(path.endsWith(".zip")) dataType = "application/zip";

    File dataFile = SPIFFS.open(path.c_str(), "r");
    
    if (ESPServer.hasArg("download")) dataType = "application/octet-stream";
    if (ESPServer.streamFile(dataFile, dataType) != dataFile.size()) {}

    dataFile.close();

    return true;
}


void ESPServer_HandleValues(void){
     String sData;
    StaticJsonDocument<2048> doc;

    // Measure Signal Strength (RSSI) of Wi-Fi connection
    lRSSI = WiFi.RSSI();
    if(lRSSI <= -100){
        doc["rssi"] = 0;
    }else if(lRSSI >= -50){
        doc["rssi"] = 100;
    }else{
        doc["rssi"] = 2 * (lRSSI + 100);
    }

    doc["MAC"] = EUtils.Get_MAC();

    if(EWifiControl.Get_Status() != WL_CONNECTED){
        doc["WifiStatus"] = "ERROR";
    }else{
        doc["WifiStatus"] = "CONNECTED";
    }
    
    doc["CSSID"] = WiFi.SSID();
    doc["CIP"] = WiFi.localIP().toString();
    doc["CGW"] = WiFi.gatewayIP().toString();
    doc["CSN"] = WiFi.subnetMask().toString();
    doc["CDNS"] = WiFi.dnsIP().toString();

    if(EWifiControl.sHostName == "NOWIFI"){
        doc["HOST Name"]  = "SS01" + EUtils.Get_MAC();
    }else{
        doc["HOST Name"]  = "SS01" + EWifiControl.sHostName;
    }    

    doc["Hostpassword"] = EWifiControl.sHostPassword;

    if(EHttpClient.Get_Internet_Status() == ESP_SERVER_OK){
        doc["Internet"]  = "OK";
    }else{
        doc["Internet"]  = "Err";
    }
    
    doc["AWN1"] = EWifiControl.sAddWifiName1;
    doc["AWP1"] = EWifiControl.sAddWifiPassword1;

    doc["AWN2"] = EWifiControl.sAddWifiName2;
    doc["AWP2"] = EWifiControl.sAddWifiPassword2;

    doc["AWN3"] = EWifiControl.sAddWifiName3;
    doc["AWP3"] = EWifiControl.sAddWifiPassword3;

    doc["AWN4"] = EWifiControl.sAddWifiName4;
    doc["AWP4"] = EWifiControl.sAddWifiPassword4;

    doc["IP1"] = EWifiControl.AddWifiIP1.toString();
    doc["GW1"] = EWifiControl.AddWifiGateway1.toString();
    doc["SN1"] = EWifiControl.AddWifiSubnet1.toString();
    doc["DN1"] = EWifiControl.AddWifiDNS1.toString();
    doc["CE1"] = EWifiControl.AddWifiConfig1.toString();
    doc["SE1"] = EWifiControl.ui8AddWifiConfigEnable1;

    doc["IP2"] = EWifiControl.AddWifiIP2.toString();
    doc["GW2"] = EWifiControl.AddWifiGateway2.toString();
    doc["SN2"] = EWifiControl.AddWifiSubnet2.toString();
    doc["DN2"] = EWifiControl.AddWifiDNS2.toString();
    doc["CE2"] = EWifiControl.AddWifiConfig2.toString();
    doc["SE2"] = EWifiControl.ui8AddWifiConfigEnable2;

    doc["IP3"] = EWifiControl.AddWifiIP3.toString();
    doc["GW3"] = EWifiControl.AddWifiGateway3.toString();
    doc["SN3"] = EWifiControl.AddWifiSubnet3.toString();
    doc["DN3"] = EWifiControl.AddWifiDNS3.toString();
    doc["CE3"] = EWifiControl.AddWifiConfig3.toString();
    doc["SE3"] = EWifiControl.ui8AddWifiConfigEnable3;

    doc["IP4"] = EWifiControl.AddWifiIP4.toString();
    doc["GW4"] = EWifiControl.AddWifiGateway4.toString();
    doc["SN4"] = EWifiControl.AddWifiSubnet4.toString();
    doc["DN4"] = EWifiControl.AddWifiDNS4.toString();
    doc["CE4"] = EWifiControl.AddWifiConfig4.toString();
    doc["SE4"] = EWifiControl.ui8AddWifiConfigEnable4;

    serializeJson(doc, sData);

    ESPServer.send(200, "text/json",sData);
}

void ESPServer_HandleRoot(void){
  ESPServer.sendHeader("Location", "/index.html",true);   //Redirect to our html web page
  ESPServer.send(302, "text/plane","");
}

void ESPServer_HandleWebRequests(void){
    if(ESPServer_LoadFromSpiffs(ESPServer.uri())){
        return;
    }
    String message = "File Not Detected\n\n";
    message += "URI: ";
    message += ESPServer.uri();
    message += "\nMethod: ";
    message += (ESPServer.method() == HTTP_GET)?"GET":"POST";
    message += "\nArguments: ";
    message += ESPServer.args();
    message += "\n";
    for (uint8_t i=0; i<ESPServer.args(); i++){
        message += " NAME:"+ESPServer.argName(i) + "\n VALUE:" + ESPServer.arg(i) + "\n";
    }

    ESPServer.send(404, "text/plain", message);
    EUtils.println(message);
}

void ESP_Handle_Wifi_Add(void){
    String sWifiName;
    String sWifiPassword;
    uint8_t ui8WifiNumber;
    String  sMsg;

    if (ESPServer.arg("username")!= ""){
        sWifiName = ESPServer.arg("username");
        if (ESPServer.arg("password")!= ""){
            sWifiPassword = ESPServer.arg("password");
        }else{
            sWifiPassword = "NoPassword";
        }

        if (ESPServer.arg("number")!= ""){
            ui8WifiNumber = ESPServer.arg("number").toInt();
            if(ui8WifiNumber > 3){
                ui8WifiNumber = 3;
            }

            if(ui8WifiNumber < 0){
                ui8WifiNumber = 0;
            }

        }else{
            ui8WifiNumber = 0;
        }

        if(EFile.DeleteFile(sESPFiles[WIFI_1_SSID_INDEX + ui8WifiNumber],&EFile.aui32ESPFileSize[WIFI_1_SSID_INDEX + ui8WifiNumber]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[WIFI_1_SSID_INDEX+ ui8WifiNumber],sWifiName) == FILE_STORE_SUCCESS){
                sMsg = "Add wifi sucess";
                if(EFile.DeleteFile(sESPFiles[WIFI_1_PASSWORD_INDEX + ui8WifiNumber],&EFile.aui32ESPFileSize[WIFI_1_PASSWORD_INDEX + ui8WifiNumber]) == FILE_DELETE_SUCCESS){
                    if(EFile.Write_Data_To_File(sESPFiles[WIFI_1_PASSWORD_INDEX + ui8WifiNumber],sWifiPassword) == FILE_STORE_SUCCESS){
                        sMsg = "Add wifi sucess";
                        EWifiControl.Update_Wifi_Add_File(ui8WifiNumber);
                    }else{
                        sMsg = "Add wifi fail";
                    }
                }else{
                    sMsg = "Fail to delete file";
                }
            }else{
                sMsg = "Add wifi fail";
            }
        }else{
           sMsg = "Fail to delete file";
        }
    }

    ESPServer.send(200, "text/plain",sMsg);
}

void ESP_Handle_Wifi_Hostname(void){
    String sMsg;

    if (ESPServer.arg("hostname")!= ""){
        if(EFile.DeleteFile(sESPFiles[WIFI_HOSTNAME_INDEX],&EFile.aui32ESPFileSize[WIFI_HOSTNAME_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[WIFI_HOSTNAME_INDEX],ESPServer.arg("hostname")) == FILE_STORE_SUCCESS){
                sMsg = "wifi hostname received";
            }else{
                sMsg = "wifi hostname failed";
            }
        }else{
           sMsg = "Fail to delete file";
        }
    }

    if (ESPServer.arg("hostpassword")!= ""){
        if(EFile.DeleteFile(sESPFiles[HOST_PASSWORD_INDEX],&EFile.aui32ESPFileSize[HOST_PASSWORD_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[HOST_PASSWORD_INDEX],ESPServer.arg("hostpassword")) == FILE_STORE_SUCCESS){
                sMsg = "wifi hostpassword received";
            }else{
                sMsg = "wifi hostpassword failed";
            }
        }else{
           sMsg = "Fail to delete file";
        }
    }

    EWifiControl.Update_Host_Name();

    ESPServer.send(200, "text/plain",sMsg);
}


void ESP_Handle_Wifi_Parameter_Add(void){
    String sIpAddr;
    String sGateway;
    String sSubnet;
    String sDNSIP;
    String sConfigEnable;
    String sMsg;
    String sWifiParamter;
    String sSubString;
    uint8_t ui8WifiIndex;
    
    sIpAddr = ESPServer.arg("ipaddr");
    sGateway = ESPServer.arg("gateway");
    sSubnet = ESPServer.arg("subnet");
    sDNSIP = ESPServer.arg("dnsip");
    sConfigEnable = ESPServer.arg("staticconfig");
    ui8WifiIndex  = ESPServer.arg("wifinumber").toInt();

    Serial1.println("file index");
    Serial1.println(ui8WifiIndex);

    if(sIpAddr.equals("A.B.C.D") == false){
        if(EWifiControl.Update_Wifi_Add_Static_Config(ui8WifiIndex,2,sIpAddr)){
            EWifiControl.Get_Wifi_Add_Parameter(ui8WifiIndex);
            sMsg  = "Wifi IP add sucess";
        }else{
            sMsg  = "Wifi IP add failed";
        }
    }

    if(sGateway.equals("A.B.C.D") == false){
        if(EWifiControl.Update_Wifi_Add_Static_Config(ui8WifiIndex,3,sGateway)){
            EWifiControl.Get_Wifi_Add_Parameter(ui8WifiIndex);
            sMsg  = "Wifi Gateway add sucess";
        }else{
            sMsg  = "Wifi Gateway add failed";
        }
    }
    
    if(sSubnet.equals("A.B.C.D") == false){
        if(EWifiControl.Update_Wifi_Add_Static_Config(ui8WifiIndex,4,sSubnet)){
            EWifiControl.Get_Wifi_Add_Parameter(ui8WifiIndex);
            sMsg  = "Wifi add sucess";
        }else{
            sMsg  = "Wifi add failed";
        }
    }

    if(sDNSIP.equals("A.B.C.D") == false){
        if(EWifiControl.Update_Wifi_Add_Static_Config(ui8WifiIndex,5,sDNSIP)){
            EWifiControl.Get_Wifi_Add_Parameter(ui8WifiIndex);
            sMsg  = "Wifi add sucess";
        }else{
            sMsg  = "Wifi add failed";
        }
    }

    if(sConfigEnable.equals("2") == false){
        if(EWifiControl.Update_Wifi_Add_Static_Config(ui8WifiIndex,1,sConfigEnable)){
            EWifiControl.Get_Wifi_Add_Parameter(ui8WifiIndex);
            sMsg  = "Wifi add sucess";
        }else{
            sMsg  = "Wifi add failed";
        }
    }

    ESPServer.send(200, "text/plain",sMsg);
    
}



void ESP_Handle_Alexa_Parameter (void){
    String sAlexaAPI;
    String sACInput1ID;
    String sACInput2ID;
    String sACInput3ID;
    String sACInput4ID;
    
    String  sMsg = " ";
    
    sAlexaAPI   = ESPServer.arg("alexaapi");
    sACInput1ID = ESPServer.arg("alexadevice1");
    sACInput2ID = ESPServer.arg("alexadevice2");
    sACInput3ID = ESPServer.arg("alexadevice3");
    sACInput4ID = ESPServer.arg("alexadevice4");
  
    EUtils.println(sAlexaAPI);
    EUtils.println(sACInput1ID);
    EUtils.println(sACInput2ID);
    EUtils.println(sACInput3ID);
    EUtils.println(sACInput4ID);
     
   if(sAlexaAPI.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[ALEXA_TOKEN_INDEX],&EFile.aui32ESPFileSize[ALEXA_TOKEN_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[ALEXA_TOKEN_INDEX],sAlexaAPI) == FILE_STORE_SUCCESS){
                sMsg = "Alexa API keys store to file sucess";
                MAlexa.Update_Specified_File(ALEXA_TOKEN_INDEX);
            }else{
                sMsg = "Alexa API keys store to file fail";
            }
       }else{
           sMsg = "Fail to delete Alexa API file";
       }
       
       //EUtils.println("File read"); 
       //EUtils.println(EFile.Read_Data_From_File(sESPFiles[ALEXA_TOKEN_INDEX]));
    }

    if(sACInput1ID.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[ALEXA_AC_1_ID_INDEX],&EFile.aui32ESPFileSize[ALEXA_AC_1_ID_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[ALEXA_AC_1_ID_INDEX],sACInput1ID) == FILE_STORE_SUCCESS){
                sMsg = sMsg + " sACInput1ID store to file sucess";
                MAlexa.Update_Specified_File(ALEXA_AC_1_ID_INDEX);
            }else{
                sMsg = sMsg + " sACInput1ID keys store to file fail";
            }
       }else{
           sMsg = sMsg + " Fail to delete sACInput1ID file";
       }
       
       //EUtils.println("File read"); 
       //EUtils.println(EFile.Read_Data_From_File(sESPFiles[sACInput1ID]));
    }

    if(sACInput2ID.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[ALEXA_AC_2_ID_INDEX],&EFile.aui32ESPFileSize[ALEXA_AC_2_ID_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[ALEXA_AC_2_ID_INDEX],sACInput2ID) == FILE_STORE_SUCCESS){
                sMsg = sMsg + " sACInput2ID store to file sucess";
                MAlexa.Update_Specified_File(ALEXA_AC_2_ID_INDEX);
            }else{
                sMsg = sMsg + " sACInput2ID keys store to file fail";
            }
       }else{
           sMsg = sMsg + " Fail to delete sACInput2ID file";
       }
       
       //EUtils.println("File read"); 
       //EUtils.println(EFile.Read_Data_From_File(sESPFiles[sACInput1ID]));
    }

    if(sACInput3ID.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[ALEXA_AC_3_ID_INDEX],&EFile.aui32ESPFileSize[ALEXA_AC_3_ID_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[ALEXA_AC_3_ID_INDEX],sACInput3ID) == FILE_STORE_SUCCESS){
                sMsg = sMsg + " sACInput3ID store to file sucess";
                MAlexa.Update_Specified_File(ALEXA_AC_3_ID_INDEX);
            }else{
                sMsg = sMsg + " sACInput3ID keys store to file fail";
            }
       }else{
           sMsg = sMsg + " Fail to delete sACInput3ID file";
       }
       
       //EUtils.println("File read"); 
       //EUtils.println(EFile.Read_Data_From_File(sESPFiles[sACInput1ID]));
    }

    if(sACInput4ID.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[ALEXA_AC_4_ID_INDEX],&EFile.aui32ESPFileSize[ALEXA_AC_4_ID_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[ALEXA_AC_4_ID_INDEX],sACInput4ID) == FILE_STORE_SUCCESS){
                sMsg = sMsg + " sACInput4ID store to file sucess";
                MAlexa.Update_Specified_File(ALEXA_AC_4_ID_INDEX);
            }else{
                sMsg = sMsg + " sACInput4ID keys store to file fail";
            }
       }else{
           sMsg = sMsg + " Fail to delete sACInput4ID file";
       }
       
       //EUtils.println("File read"); 
       //EUtils.println(EFile.Read_Data_From_File(sESPFiles[sACInput1ID]));
    }

    if(sMsg.equals(" ")){
        sMsg = "no action taken ";
    }

    EUtils.println(sMsg);

    ESPServer.send(200, "text/plain",sMsg);
}


void ESP_Handle_DeviceReset_Parameter (void){
    String sReset;
   
    String  sMsg = " ";
    
    sReset   = ESPServer.arg("value");
    
    if(sReset.equals("1")){
        sMsg = "device will resetart now";
        ui8DeviceResetFlag=1;
    }

    if(sMsg.equals(" ")){
        sMsg = "no action taken ";
    }

    EUtils.println(sMsg);

    ESPServer.send(200, "text/plain",sMsg);
}

void ESP_Handle_Alexa_Values(void){
    String sData;
    StaticJsonDocument<1024> doc;

    doc["APIKey"] = MAlexa.sAlexaAPIKey;
    doc["ACInput1ID"] = MAlexa.sAlexaACInput1ID;
    doc["ACInput2ID"] = MAlexa.sAlexaACInput2ID;
    doc["ACInput3ID"] = MAlexa.sAlexaACInput3ID;
    doc["ACInput4ID"] = MAlexa.sAlexaACInput4ID;
    doc["MqttServer"] = AC.sMqttServer;
    doc["MqttPort"] = AC.ui16MqttPort;
    doc["MqttClient"] = AC.sMqttClientId;
    doc["MqttUsername"] = AC.sMqttUsername;
    doc["MqttPassword"] = AC.sMqttPassword;
    doc["MqttQos"] = AC.ui8MqttQoS;
    doc["MqttKeepalive"] = AC.ui16MqttKeepalive;
    doc["AC1Topic"] = AC.sMqttPubAC1Topic;
    doc["AC2Topic"] = AC.sMqttPubAC2Topic;
    doc["AC3Topic"] = AC.sMqttPubAC3Topic;
    doc["AC4Topic"] = AC.sMqttPubAC4Topic;
    doc["ACAlltopic"] = AC.sMqttSubACAllTopic;
    doc["ACInput1Name"] = MAlexa.sAlexaACInput1Name;
    doc["ACInput2Name"] = MAlexa.sAlexaACInput2Name;
    doc["ACInput3Name"] = MAlexa.sAlexaACInput3Name;
    doc["ACInput4Name"] = MAlexa.sAlexaACInput4Name;
    doc["ACInput1Type"] = AC.Get_ACControlType_In_String(AC_INPUT_1_INDEX);
    doc["ACInput2Type"] = AC.Get_ACControlType_In_String(AC_INPUT_2_INDEX);
    doc["ACInput3Type"] = AC.Get_ACControlType_In_String(AC_INPUT_3_INDEX);
    doc["ACInput4Type"] = AC.Get_ACControlType_In_String(AC_INPUT_4_INDEX);
    doc["ACInput1Value"] = AC.Get_ACControlValue(AC_INPUT_1_INDEX);
    doc["ACInput2Value"] = AC.Get_ACControlValue(AC_INPUT_2_INDEX);
    doc["ACInput3Value"] = AC.Get_ACControlValue(AC_INPUT_3_INDEX);
    doc["ACInput4Value"] = AC.Get_ACControlValue(AC_INPUT_4_INDEX);
    serializeJson(doc, sData);

    ESPServer.send(200, "text/json",sData);
}

void ESP_Handle_AC_Values(void){
    String sData;
    StaticJsonDocument<600> doc;

    doc["ac1image"] = AC.Get_AC_Image(AC_INPUT_1_INDEX);
    doc["ac2image"] = AC.Get_AC_Image(AC_INPUT_2_INDEX);
    doc["ac3image"] = AC.Get_AC_Image(AC_INPUT_3_INDEX);
    doc["ac4image"] = AC.Get_AC_Image(AC_INPUT_4_INDEX);

    doc["ac1value"] = AC.Get_ACControlValue(AC_INPUT_1_INDEX);
    doc["ac2value"] = AC.Get_ACControlValue(AC_INPUT_2_INDEX);
    doc["ac3value"] = AC.Get_ACControlValue(AC_INPUT_3_INDEX);
    doc["ac4value"] = AC.Get_ACControlValue(AC_INPUT_4_INDEX);

    doc["ac1name"] = MAlexa.sAlexaACInput1Name;
    doc["ac2name"] = MAlexa.sAlexaACInput2Name;
    doc["ac3name"] = MAlexa.sAlexaACInput3Name;
    doc["ac4name"] = MAlexa.sAlexaACInput4Name;

    doc["ac1type"] = AC.Get_ACControlType_In_String(AC_INPUT_1_INDEX);
    doc["ac2type"] = AC.Get_ACControlType_In_String(AC_INPUT_2_INDEX);
    doc["ac3type"] = AC.Get_ACControlType_In_String(AC_INPUT_3_INDEX);
    doc["ac4type"] = AC.Get_ACControlType_In_String(AC_INPUT_4_INDEX);

    serializeJson(doc, sData);

    ESPServer.send(200, "text/json",sData);
}


void ESP_Handle_AC_Websetvalues(void){
    String sACinputvalue1;
    String sACinputvalue2;
    String sACinputvalue3;
    String sACinputvalue4;
    String sMsg;
    uint8_t ui8ACinputValue1;
    uint8_t ui8ACinputValue2;
    uint8_t ui8ACinputValue3;
    uint8_t ui8ACinputValue4;

    sACinputvalue1 = ESPServer.arg("acin1value");
    sACinputvalue2 = ESPServer.arg("acin2value");
    sACinputvalue3 = ESPServer.arg("acin3value");
    sACinputvalue4 = ESPServer.arg("acin4value");

    EUtils.println(sACinputvalue1);
    EUtils.println(sACinputvalue2);
    EUtils.println(sACinputvalue3);
    EUtils.println(sACinputvalue4);
    

    if(sACinputvalue1.equals("65535") == false){
        ui8ACinputValue1 = sACinputvalue1.toInt();
        if(ui8ACinputValue1 > 100){
            ui8ACinputValue1 = 100;
        }

        if(ui8ACinputValue1 < 0){
            ui8ACinputValue1 = 0;
        }

        if(AC.Set_ACControlValue(AC_INPUT_1_INDEX,ui8ACinputValue1)){
            sMsg = "AC input 1 value store sucess";
        }else{
            sMsg = "AC input 1 value store fail";
        }
    }

    if(sACinputvalue2.equals("65535") == false){
        ui8ACinputValue2 = sACinputvalue2.toInt();
        if(ui8ACinputValue2 > 100){
            ui8ACinputValue2 = 100;
        }

        if(ui8ACinputValue2 < 0){
            ui8ACinputValue2 = 0;
        }

        if(AC.Set_ACControlValue(AC_INPUT_2_INDEX,ui8ACinputValue2)){
            sMsg = "AC input 2 value store sucess";
        }else{
            sMsg = "AC input 2 value store fail";
        }
    }

    if(sACinputvalue3.equals("65535") == false){
        ui8ACinputValue3 = sACinputvalue3.toInt();
        if(ui8ACinputValue3 > 100){
            ui8ACinputValue3 = 100;
        }

        if(ui8ACinputValue3 < 0){
            ui8ACinputValue3 = 0;
        }

        if(AC.Set_ACControlValue(AC_INPUT_3_INDEX,ui8ACinputValue3)){
            sMsg = "AC input 3 value store sucess";
        }else{
            sMsg = "AC input 3 value store fail";
        }
    }

    if(sACinputvalue4.equals("65535") == false){
        ui8ACinputValue4 = sACinputvalue4.toInt();
        if(ui8ACinputValue4 > 100){
            ui8ACinputValue4 = 100;
        }

        if(ui8ACinputValue4 < 0){
            ui8ACinputValue4 = 0;
        }

        if(AC.Set_ACControlValue(AC_INPUT_4_INDEX,ui8ACinputValue4)){
            sMsg = "AC input 4 value store sucess";
        }else{
            sMsg = "AC input 4 value store fail";
        }
    }

    ESPServer.send(200, "text/plain",sMsg);

}     

void ESP_Handle_Mqtt_Parameter (void){
    String sMqttServer;
	String sMqttPort;
	String sMqttClientId;
	String sMqttUsername;
	String sMqttPassword;
	String sMqttAC1Topic;
	String sMqttAC2Topic;
	String sMqttAC3Topic;
	String sMqttAC4Topic;
	String sMqttACAllTopic;
    String sQos;
    String sKeepalive;
    String  sMsg = " ";
    
    sMqttServer     = ESPServer.arg("sMqttServer");
    sMqttPort       = ESPServer.arg("sMqttPort");
    sMqttClientId   = ESPServer.arg("sMqttClientId");
    sMqttUsername   = ESPServer.arg("sMqttUsername");
    sMqttPassword   = ESPServer.arg("sMqttPassword");
    sMqttAC1Topic   =  ESPServer.arg("sMqttAC1Topic");
    sMqttAC2Topic   = ESPServer.arg("sMqttAC2Topic");
    sMqttAC3Topic   = ESPServer.arg("sMqttAC3Topic");
    sMqttAC4Topic   = ESPServer.arg("sMqttAC4Topic");
    sMqttACAllTopic = ESPServer.arg("sMqttACAllTopic");
    sQos            = ESPServer.arg("sQos");
    sKeepalive      = ESPServer.arg("sMqttKeepalive");
    
    EUtils.println(sMqttServer);
    EUtils.println(sMqttPort);
    EUtils.println(sMqttClientId);
    EUtils.println(sMqttUsername);
    EUtils.println(sMqttPassword);
    EUtils.println(sMqttAC1Topic);
    EUtils.println(sMqttAC2Topic);
    EUtils.println(sMqttAC3Topic);
    EUtils.println(sMqttAC4Topic);
    EUtils.println(sMqttACAllTopic);
    EUtils.println(sQos);
    EUtils.println(sKeepalive);
    
   
    if(sMqttServer.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[MQTT_SERVER_INDEX],&EFile.aui32ESPFileSize[MQTT_SERVER_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[MQTT_SERVER_INDEX],sMqttServer) == FILE_STORE_SUCCESS){
                sMsg = "mqtt server to file sucess";
                AC.Update_Mqtt_Parameter(MQTT_SERVER_INDEX);
            }else{
                sMsg = "mqtt server store to file fail";
            }
       }else{
           sMsg = "Fail to delete mqtt server file";
       }
    }

    if(sMqttPort.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[MQTT_PORT_INDEX],&EFile.aui32ESPFileSize[MQTT_PORT_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[MQTT_PORT_INDEX],sMqttPort) == FILE_STORE_SUCCESS){
                sMsg = "mqtt port to file sucess";
                AC.Update_Mqtt_Parameter(MQTT_PORT_INDEX);
            }else{
                sMsg = "mqtt port store to file fail";
            }
       }else{
           sMsg = "Fail to delete mqtt port file";
       }
    }

    if(sMqttClientId.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[MQTT_CLIENTID_INDEX],&EFile.aui32ESPFileSize[MQTT_CLIENTID_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[MQTT_CLIENTID_INDEX],sMqttClientId) == FILE_STORE_SUCCESS){
                sMsg = "mqtt clientid to file sucess";
                AC.Update_Mqtt_Parameter(MQTT_CLIENTID_INDEX);
            }else{
                sMsg = "mqtt clientid store to file fail";
            }
       }else{
           sMsg = "Fail to delete mqtt clientid file";
       }
    }

    if(sMqttUsername.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[MQTT_USERNAME_INDEX],&EFile.aui32ESPFileSize[MQTT_USERNAME_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[MQTT_USERNAME_INDEX],sMqttUsername) == FILE_STORE_SUCCESS){
                sMsg = "mqtt username to file sucess";
                AC.Update_Mqtt_Parameter(MQTT_USERNAME_INDEX);
            }else{
                sMsg = "mqtt username store to file fail";
            }
       }else{
           sMsg = "Fail to delete mqtt username file";
       }
    }


    if(sMqttPassword.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[MQTT_PASSWORD_INDEX],&EFile.aui32ESPFileSize[MQTT_PASSWORD_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[MQTT_PASSWORD_INDEX],sMqttPassword) == FILE_STORE_SUCCESS){
                sMsg = "mqtt password to file sucess";
                AC.Update_Mqtt_Parameter(MQTT_PASSWORD_INDEX);
            }else{
                sMsg = "mqtt password store to file fail";
            }
       }else{
           sMsg = "Fail to delete mqtt password file";
       }
    }

    if(sMqttAC1Topic.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[MQTT_PUB_AC_1_TOPIC_INDEX],&EFile.aui32ESPFileSize[MQTT_PUB_AC_1_TOPIC_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[MQTT_PUB_AC_1_TOPIC_INDEX],sMqttAC1Topic) == FILE_STORE_SUCCESS){
                sMsg = "mqtt ac1topic to file sucess";
                AC.Update_Mqtt_Parameter(MQTT_PUB_AC_1_TOPIC_INDEX);
            }else{
                sMsg = "mqtt ac1topic store to file fail";
            }
       }else{
           sMsg = "Fail to delete mqtt ac1topic file";
       }
    }

    if(sMqttAC2Topic.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[MQTT_PUB_AC_2_TOPIC_INDEX],&EFile.aui32ESPFileSize[MQTT_PUB_AC_2_TOPIC_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[MQTT_PUB_AC_2_TOPIC_INDEX],sMqttAC2Topic) == FILE_STORE_SUCCESS){
                sMsg = "mqtt ac2topic to file sucess";
                AC.Update_Mqtt_Parameter(MQTT_PUB_AC_2_TOPIC_INDEX);
            }else{
                sMsg = "mqtt ac2topic store to file fail";
            }
       }else{
           sMsg = "Fail to delete mqtt ac2topic file";
       }
    }

    if(sMqttAC3Topic.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[MQTT_PUB_AC_3_TOPIC_INDEX],&EFile.aui32ESPFileSize[MQTT_PUB_AC_3_TOPIC_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[MQTT_PUB_AC_3_TOPIC_INDEX],sMqttAC3Topic) == FILE_STORE_SUCCESS){
                sMsg = "mqtt ac3topic to file sucess";
                AC.Update_Mqtt_Parameter(MQTT_PUB_AC_3_TOPIC_INDEX);
            }else{
                sMsg = "mqtt ac3topic store to file fail";
            }
       }else{
           sMsg = "Fail to delete mqtt ac3topic file";
       }
    }

    if(sMqttAC4Topic.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[MQTT_PUB_AC_4_TOPIC_INDEX],&EFile.aui32ESPFileSize[MQTT_PUB_AC_4_TOPIC_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[MQTT_PUB_AC_4_TOPIC_INDEX],sMqttAC4Topic) == FILE_STORE_SUCCESS){
                sMsg = "mqtt ac4topic to file sucess";
                AC.Update_Mqtt_Parameter(MQTT_PUB_AC_4_TOPIC_INDEX);
            }else{
                sMsg = "mqtt ac4topic store to file fail";
            }
       }else{
           sMsg = "Fail to delete mqtt ac4topic file";
       }
    }

    if(sMqttACAllTopic.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[MQTT_SUB_AC_TOPIC_INDEX],&EFile.aui32ESPFileSize[MQTT_SUB_AC_TOPIC_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[MQTT_SUB_AC_TOPIC_INDEX],sMqttACAllTopic) == FILE_STORE_SUCCESS){
                sMsg = "mqtt acalltopic to file sucess";
                AC.Update_Mqtt_Parameter(MQTT_SUB_AC_TOPIC_INDEX);
            }else{
                sMsg = "mqtt acalltopic store to file fail";
            }
       }else{
           sMsg = "Fail to delete mqtt acalltopic file";
       }
    }

    if((sQos.equals("")== false) and (sQos.equals("4")== false)){
        
       if(EFile.DeleteFile(sESPFiles[MQTT_QOS_INDEX],&EFile.aui32ESPFileSize[MQTT_QOS_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[MQTT_QOS_INDEX],sQos) == FILE_STORE_SUCCESS){
                sMsg = "mqtt qos to file sucess";
                AC.Update_Mqtt_Parameter(MQTT_QOS_INDEX);
            }else{
                sMsg = "mqtt qos store to file fail";
            }
       }else{
           sMsg = "Fail to delete mqtt qos file";
       }
    }


    if(sKeepalive.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[MQTT_KEEPALIVE_INDEX],&EFile.aui32ESPFileSize[MQTT_KEEPALIVE_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[MQTT_KEEPALIVE_INDEX],sKeepalive) == FILE_STORE_SUCCESS){
                sMsg = "mqtt keepalive to file sucess";
                AC.Update_Mqtt_Parameter(MQTT_KEEPALIVE_INDEX);
            }else{
                sMsg = "mqtt keepalive store to file fail";
            }
       }else{
           sMsg = "Fail to delete mqtt keepalive file";
       }
    }

    if(sMsg.equals(" ")){
        sMsg = "no action taken ";
    }

    EUtils.println(sMsg);

    ESPServer.send(200, "text/plain",sMsg);
}

void ESP_Handle_Device_Parameter (void){
    String sACInput1Name;
    String sACInput2Name;
    String sACInput3Name;
    String sACInput4Name;
    uint8_t ui8ACInputType1;
    uint8_t ui8ACInputType2;
    uint8_t ui8ACInputType3;
    uint8_t ui8ACInputType4;
    String  sMsg = " ";
    
    sACInput1Name = ESPServer.arg("devicename1");
    sACInput2Name = ESPServer.arg("devicename2");
    sACInput3Name = ESPServer.arg("devicename3");
    sACInput4Name = ESPServer.arg("devicename4");
    ui8ACInputType1 = (ESPServer.arg("devicetype1")).toInt();
    ui8ACInputType2 = (ESPServer.arg("devicetype2")).toInt();
    ui8ACInputType3 = (ESPServer.arg("devicetype3")).toInt();
    ui8ACInputType4 = (ESPServer.arg("devicetype4")).toInt();

    EUtils.println(sACInput1Name);
    EUtils.println(sACInput2Name);
    EUtils.println(sACInput3Name);
    EUtils.println(sACInput4Name);
    EUtils.println(ui8ACInputType1);
    EUtils.println(ui8ACInputType2);
    EUtils.println(ui8ACInputType3);
    EUtils.println(ui8ACInputType4);
    

    if(sACInput1Name.equals("")== false){
       if(EFile.DeleteFile(sESPFiles[ALEXA_AC_1_NAME_INDEX],&EFile.aui32ESPFileSize[ALEXA_AC_1_NAME_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[ALEXA_AC_1_NAME_INDEX],sACInput1Name) == FILE_STORE_SUCCESS){
                sMsg = "sACInput1Name store to file sucess";
                MAlexa.Update_Specified_File(ALEXA_AC_1_NAME_INDEX);
            }else{
                sMsg = "sACInput1Name keys store to file fail";
            }
       }else{
           sMsg = "Fail to delete file";
       }
       
       //EUtils.println("File read"); 
       //EUtils.println(EFile.Read_Data_From_File(sESPFiles[sACInput1ID]));
    }

    if(sACInput2Name.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[ALEXA_AC_2_NAME_INDEX],&EFile.aui32ESPFileSize[ALEXA_AC_2_NAME_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[ALEXA_AC_2_NAME_INDEX],sACInput2Name) == FILE_STORE_SUCCESS){
                sMsg = sMsg + " sACInput2Name store to file sucess";
                MAlexa.Update_Specified_File(ALEXA_AC_2_NAME_INDEX);
            }else{
                sMsg =sMsg + " sACInput2Name keys store to file fail";
            }
       }else{
           sMsg = sMsg + " Fail to delete file";
       }
       
       //EUtils.println("File read"); 
       //EUtils.println(EFile.Read_Data_From_File(sESPFiles[sACInput1ID]));
    }

    if(sACInput4Name.equals("")== false){
       if(EFile.DeleteFile(sESPFiles[ALEXA_AC_4_NAME_INDEX],&EFile.aui32ESPFileSize[ALEXA_AC_4_NAME_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[ALEXA_AC_4_NAME_INDEX],sACInput4Name) == FILE_STORE_SUCCESS){
                sMsg = sMsg + " sACInput4Name store to file sucess";
                MAlexa.Update_Specified_File(ALEXA_AC_4_NAME_INDEX);
            }else{
                sMsg = sMsg + " sACInput4Name keys store to file fail";
            }
       }else{
           sMsg = sMsg + " Fail to delete file";
       }
       
       //EUtils.println("File read"); 
       //EUtils.println(EFile.Read_Data_From_File(sESPFiles[sACInput1ID]));
    }

    if(sACInput3Name.equals("")== false){

       if(EFile.DeleteFile(sESPFiles[ALEXA_AC_3_NAME_INDEX],&EFile.aui32ESPFileSize[ALEXA_AC_3_NAME_INDEX]) == FILE_DELETE_SUCCESS){
            if(EFile.Write_Data_To_File(sESPFiles[ALEXA_AC_3_NAME_INDEX],sACInput3Name) == FILE_STORE_SUCCESS){
                sMsg = sMsg + " sACInput3Name store to file sucess";
                MAlexa.Update_Specified_File(ALEXA_AC_3_NAME_INDEX);
            }else{
                sMsg = sMsg + " sACInput3Name keys store to file fail";
            }
       }else{
           sMsg = sMsg + " Fail to delete file";
       }
       
       //EUtils.println("File read"); 
       //EUtils.println(EFile.Read_Data_From_File(sESPFiles[sACInput1ID]));
    }
    
    if(ui8ACInputType1 != 0){
        if(AC.Set_ACControlType(AC_INPUT_1_INDEX,ui8ACInputType1)){
            sMsg = sMsg + " AC input 1 type store sucess";
        }else{
            sMsg = sMsg + " AC input 1 type store fail";
        }
    }
    
    if(ui8ACInputType2 != 0){
        if(AC.Set_ACControlType(AC_INPUT_2_INDEX,ui8ACInputType2)){
            sMsg = sMsg + " AC input 2 type store sucess";
        }else{
            sMsg = sMsg + " AC input 2 type store fail";
        }
    }

    if(ui8ACInputType3 != 0){
        if(AC.Set_ACControlType(AC_INPUT_3_INDEX,ui8ACInputType3)){
            sMsg = sMsg + " AC input 3 type store sucess";
        }else{
            sMsg = sMsg + " AC input 3 type store fail";
        }
    }

    if(ui8ACInputType4 != 0){
        if(AC.Set_ACControlType(AC_INPUT_4_INDEX,ui8ACInputType4)){
            sMsg = sMsg + " AC input 4 type store sucess";
        }else{
            sMsg = sMsg + " AC input 4 type store fail";
        }
    }

    if(sMsg.equals(" ")){
        sMsg = "no action taken ";
    }

    EUtils.println(sMsg);

    ESPServer.send(200, "text/plain",sMsg);
}

void ESP_Handle_AC_Testvalues(void){
    String sACTestvalue;
    String sMsg;
    
    sACTestvalue = ESPServer.arg("actestvalue");
    
    if(sACTestvalue.equals("65535") == false){
        AC.ui16CodeTestValue = sACTestvalue.toInt();
        sMsg = "test value added";
    }

    ESPServer.send(200, "text/plain",sMsg);

} 

void MQTT_Handle(void) {
    if(ui8MqttSendFlag){
        EUtils.println("Mqtt handle publish all");
        mqttClient.publish(AC.sMqttPubAC1Topic.c_str(), AC.ui8MqttQoS, true,String(AC.Get_Mqtt_AC_String(AC_INPUT_1_INDEX)).c_str());

        mqttClient.publish(AC.sMqttPubAC2Topic.c_str(), AC.ui8MqttQoS, true,String(AC.Get_Mqtt_AC_String(AC_INPUT_2_INDEX)).c_str());

        mqttClient.publish(AC.sMqttPubAC3Topic.c_str(), AC.ui8MqttQoS, true,String(AC.Get_Mqtt_AC_String(AC_INPUT_3_INDEX)).c_str());

        mqttClient.publish(AC.sMqttPubAC4Topic.c_str(), AC.ui8MqttQoS, true,String(AC.Get_Mqtt_AC_String(AC_INPUT_4_INDEX)).c_str());
    }

}


void onMqttPublish(uint16_t packetId) {
  Serial1.println("Publish acknowledged.");
  
  //Serial1.print("  packetId: ");
  //Serial1.println(packetId);
  //sEndTime = micros();
  //Serial1.println(sEndTime - sStratTime);
}

void onMqttMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
  Serial1.println("Publish received.");
  Serial1.print("  topic: ");
  Serial1.println(topic);
  Serial1.print("  qos: ");
  Serial1.println(properties.qos);
  Serial1.print("  dup: ");
  Serial1.println(properties.dup);
  Serial1.print("  retain: ");
  Serial1.println(properties.retain);
  Serial1.print("  len: ");
  Serial1.println(len);
  Serial1.print("  index: ");
  Serial1.println(index);
  Serial1.print("  total: ");
  Serial1.println(total);
  Serial1.println(payload);
     
    uint8_t ui8Payload;

    ui8Payload = String(payload).toInt();
    if(ui8Payload > 100){
        ui8Payload = 100;
    }
    
    if(ui8Payload < 0){
        ui8Payload = 0;
    }

    if(strcmp(topic,AC.sMqttSubACAllTopic.c_str()) == 0){
        Mqtt_Publish_All();
    }

    if(strcmp(topic,AC.sMqttSubAC1Topic.c_str()) == 0){
        AC.Set_ACControlValue(AC_INPUT_1_INDEX,ui8Payload);
        Mqtt_Publish_All();
    }
    
    if(strcmp(topic,AC.sMqttSubAC2Topic.c_str()) == 0){
        AC.Set_ACControlValue(AC_INPUT_2_INDEX,ui8Payload);
        Mqtt_Publish_All();
    }

    if(strcmp(topic,AC.sMqttSubAC3Topic.c_str()) == 0){
        AC.Set_ACControlValue(AC_INPUT_3_INDEX,ui8Payload);
        Mqtt_Publish_All();
    }

    if(strcmp(topic,AC.sMqttSubAC4Topic.c_str()) == 0){
        AC.Set_ACControlValue(AC_INPUT_4_INDEX,ui8Payload);
        Mqtt_Publish_All();
    }
}

void onMqttUnsubscribe(uint16_t packetId) {
  Serial1.println("Unsubscribe acknowledged.");
  Serial1.print("  packetId: ");
  Serial1.println(packetId);
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos) {
  Serial1.println("Subscribe acknowledged.");
  Serial1.print("  packetId: ");
  Serial1.println(packetId);
  Serial1.print("  qos: ");
  Serial1.println(qos);
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
    Serial1.print("Disconnected from MQTT Reason: ");
    Serial1.printf("%d",reason);
    Serial1.println();
    
    ui8MqttSendFlag = 0;
    EHttpClient.ui8MetiServerStatus = APP_SERVER_ERROR;
    if (WiFi.isConnected()) {
        connectToMqtt();
        
        //MqttReconnectTask.enable();
    }
}

void connectToMqtt(void) {
  Serial1.println("Connecting to MQTT...");
  mqttClient.setClientId(AC.sMqttClientId.c_str());
  mqttClient.setCredentials(AC.sMqttUsername.c_str(),AC.sMqttPassword.c_str());
  mqttClient.setServer(AC.sMqttServer.c_str(),AC.ui16MqttPort);
  mqttClient.setKeepAlive(AC.ui16MqttKeepalive);
  mqttClient.setWill(AC.sMqttDeviceStatus.c_str(),AC.ui8MqttQoS,1,"offline",strlen("offline"));
  mqttClient.connect();
}

void onMqttConnect(bool sessionPresent) {
    
    Serial1.println("Connected to MQTT.");
    Serial1.print("Session present: ");
    Serial1.println(sessionPresent);

    ui8MqttSendFlag = 1;
    EHttpClient.ui8MetiServerStatus = APP_SERVER_OK;

    mqttClient.subscribe(AC.sMqttSubAC1Topic.c_str(),AC.ui8MqttQoS);
    mqttClient.subscribe(AC.sMqttSubAC2Topic.c_str(),AC.ui8MqttQoS);
    mqttClient.subscribe(AC.sMqttSubAC3Topic.c_str(),AC.ui8MqttQoS);
    mqttClient.subscribe(AC.sMqttSubAC4Topic.c_str(),AC.ui8MqttQoS);
    mqttClient.subscribe(AC.sMqttSubACAllTopic.c_str(),AC.ui8MqttQoS);
    
    Mqtt_Publish_All();
    mqttClient.publish(AC.sMqttDeviceStatus.c_str(), AC.ui8MqttQoS, true,"online");

    
}

void Mqtt_Publish_All(void){
    mqttClient.publish(AC.sMqttPubAC1Topic.c_str(), AC.ui8MqttQoS, true,String(AC.Get_Mqtt_AC_String(AC_INPUT_1_INDEX)).c_str());

    mqttClient.publish(AC.sMqttPubAC2Topic.c_str(), AC.ui8MqttQoS, true,String(AC.Get_Mqtt_AC_String(AC_INPUT_2_INDEX)).c_str());

    mqttClient.publish(AC.sMqttPubAC3Topic.c_str(), AC.ui8MqttQoS, true,String(AC.Get_Mqtt_AC_String(AC_INPUT_3_INDEX)).c_str());

    mqttClient.publish(AC.sMqttPubAC4Topic.c_str(), AC.ui8MqttQoS, true,String(AC.Get_Mqtt_AC_String(AC_INPUT_4_INDEX)).c_str());

    Serial1.println("Publish all");
}

void Mqtt_Check_For_Alexa_Sync(void){
    if(EApp.ui8AlexaUpdated){
        EUtils.println("Alexa mqtt sync");
        EApp.ui8AlexaUpdated=0;
        MQTT_Handle();
    }
}