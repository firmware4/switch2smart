/*******************************************************************************
*File Name: ESPApplication.cpp
*
* Version: 1.0
*
* Description:
* In this source code for managing applications
*
*
* Owner:
* silicosmos
*
********************************************************************************
* Copyright (2020-21) , silicosmos.in
*******************************************************************************/
#include <Arduino.h>
#include <Application.h>
#include <ESP8266WebServer.h>
#include <FS.h>
#include <ESPUtils.h>
#include <ESPMemory.h>
#include <ESPFile.h>
#include <ArduinoJson.h>
#include <WebSocketsClient.h> 
#include <ESPAlexaV2.h>
#include <ACControl.h>

// Constructors ////////////////////////////////////////////////////////////////
ESPApplication::ESPApplication(){}

void ESPApplication::HandleAlexa(uint8_t * payload){
    DynamicJsonDocument json(1024);
    deserializeJson(json, payload,DeserializationOption::NestingLimit(20));
    uint8_t ui8DeviceIndex=0;

    String sDeviceId = json ["deviceId"];
    String sAction = json ["action"];
    String sValue = json["value"];
    int    i16jsonValue=0;
    int ai16ResponseData[3];

    ai16ResponseData[0] = 0x7FFF;
    ai16ResponseData[1] = 0x7FFF;
    ai16ResponseData[2] = 1;

    if(sAction == "SetPercentage"){
        i16jsonValue = json["value"]["percentage"];
    }else if(sAction == "AdjustPercentage"){
        i16jsonValue = json["value"]["percentageDelta"];
    }else if(sAction == "action.devices.commands.OnOff"){
        String sString1 = json["value"]["on"];
        EUtils.println(sString1);
        if(sString1 == "true"){
            i16jsonValue = 1;
            EUtils.println("OK");
        }else{
            i16jsonValue = 0;
            EUtils.println("Error");
        }
    }else if(sAction == "action.devices.commands.BrightnessAbsolute"){
        String sString1 = json["value"]["brightness"];
        EUtils.println(sString1);
        i16jsonValue = json["value"]["brightness"];
        EUtils.println("brigthness");
        EUtils.println(i16jsonValue);
    }else{
        i16jsonValue =  0;
    }

    EUtils.print("Sinric data: ");
    EUtils.print(sDeviceId);
    EUtils.print(" ");
    EUtils.print(sAction);
    EUtils.print(" ");
    EUtils.print(sValue);
    EUtils.print(" ");
    EUtils.println(i16jsonValue);

    //serializeJsonPretty(json,sString);
    ui8DeviceIndex = Check_For_Device_ID(sDeviceId);

    if(ui8DeviceIndex != 0xFF){
        Check_For_Action(sAction,sValue,i16jsonValue,ai16ResponseData);
        EUtils.print("ai16ResponseData[0]:");
        EUtils.println(ai16ResponseData[0]);
        EUtils.print("ai16ResponseData[1]:");
        EUtils.println(ai16ResponseData[1]);
        Update_AC_Input_Values(ui8DeviceIndex, ai16ResponseData);
    }else{
        EUtils.print("No config id:");
        EUtils.println(sDeviceId);
    }
}

uint8_t ESPApplication::Check_For_Device_ID(String sDeviceID){
    uint8_t ui8Index=0xFF;
 
    if(sDeviceID.equals(MAlexa.sAlexaACInput1ID)){
        //EUtils.print("Device is correct:");
        //EUtils.println(MAlexa.sAlexaACInput1ID);
        EUtils.println(MAlexa.sAlexaACInput1Name);
        ui8Index =  AC_INPUT_1_INDEX;
    }else if(sDeviceID.equals(MAlexa.sAlexaACInput2ID)){
        //EUtils.print("Device is correct:");
        //EUtils.println(MAlexa.sAlexaACInput2ID);
        EUtils.println(MAlexa.sAlexaACInput2Name);
        ui8Index = AC_INPUT_2_INDEX;
    }else if(sDeviceID.equals(MAlexa.sAlexaACInput3ID)){
        //EUtils.print("Device is correct:");
        //EUtils.println(MAlexa.sAlexaACInput3ID);
        ui8Index = AC_INPUT_3_INDEX;
        EUtils.println(MAlexa.sAlexaACInput3Name);
    }else if(sDeviceID.equals(MAlexa.sAlexaACInput4ID)){
        //EUtils.print("Device is correct:");
        //EUtils.println(MAlexa.sAlexaACInput4ID);
        ui8Index = AC_INPUT_4_INDEX;
        EUtils.println(MAlexa.sAlexaACInput4Name);
    }else{
        ui8Index=0xFF;
    }

    return ui8Index;
}

void ESPApplication::Check_For_Action(String sAction, String sValue,int i16jsonValue, int* ai16ResponseData){
    int i16Value=0;

    if(sAction.equals("setPowerState")){
        
        if(sValue.equals(DEVICE_ON)){
            ai16ResponseData[0] = 100;
        }else{
            ai16ResponseData[0] = 0;
        }
    }else if(sAction.equals("SetBrightness")){
        i16Value = sValue.toInt();
        if(i16Value > 100){
            i16Value = 100;
        }
        if(i16Value < 0){
            i16Value = 0;
        }
        ai16ResponseData[0] = i16Value;
    }else if(sAction.equals("SetPercentage")){
        ai16ResponseData[0] = i16jsonValue;
    }else if(sAction.equals("AdjustBrightness")){
        ai16ResponseData[1] = sValue.toInt();
    }else if(sAction.equals("AdjustPercentage")){
        ai16ResponseData[1] = i16jsonValue;
    }else if(sAction == "action.devices.commands.OnOff"){
        if(i16jsonValue){
            ai16ResponseData[0] = 100;
        }else{
            ai16ResponseData[0] = 0;
        }
    }else if(sAction == "action.devices.commands.BrightnessAbsolute"){
        if(i16jsonValue > 100){
            i16jsonValue = 100;
        }
        if(i16jsonValue < 0){
            i16jsonValue = 0;
        }
        ai16ResponseData[0] = i16jsonValue;
    }else{
        // do nothing
        ai16ResponseData[2] = 0;
    }
}

void ESPApplication::Update_AC_Input_Values(uint8_t ui8DeviceIndex, int* ai16ResponseData){
    uint8_t ui8GetCurrentValue=0;
    if(ui8DeviceIndex < 4){
        if(ai16ResponseData[2]){
            if(ai16ResponseData[1] == 0x7FFF){
                if(ai16ResponseData[0] == 0x7FFF){
                    ai16ResponseData[0] = 0;
                }
            }else{
                ui8GetCurrentValue = AC.Get_ACControlValue(ui8DeviceIndex);
                ai16ResponseData[0] = ui8GetCurrentValue + ai16ResponseData[1];
            }

            if((ai16ResponseData[0] >= 0) and (ai16ResponseData[0] <= 100)){
                AC.Set_ACControlValue(ui8DeviceIndex,ai16ResponseData[0]);
                EUtils.print("OK ");
                EUtils.println(AC.Get_ACControlValue(ui8DeviceIndex));
                ui8AlexaUpdated=1;
            }else if(ai16ResponseData[0] > 100){
                AC.Set_ACControlValue(ui8DeviceIndex,100);
                EUtils.print(">100 ");
                EUtils.println(AC.Get_ACControlValue(ui8DeviceIndex));
                ui8AlexaUpdated=1;
            }else{
                AC.Set_ACControlValue(ui8DeviceIndex,0);
                EUtils.print("<0 ");
                EUtils.println(AC.Get_ACControlValue(ui8DeviceIndex));
                ui8AlexaUpdated=1;
            }
        }
    }
}

// Preinstantiate Objects //////////////////////////////////////////////////////
#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_TWOWIRE)
ESPApplication EApp;
#endif