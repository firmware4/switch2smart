/*******************************************************************************
*File Name: Application.h
*
* Version: 1.0
*
* Description:
* In this header used for managing applications
*
*
* Owner:
* silicosmos
*
********************************************************************************
* Copyright (2020-21) , silicosmos.in
*******************************************************************************/
#ifndef APPLICATION_h
#define APPLICATION_h
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <FS.h>

class ESPApplication{
public:
    uint8_t ui8AlexaUpdated=0;
    ESPApplication();
    void HandleAlexa(uint8_t * payload);
    uint8_t Check_For_Device_ID(String sDeviceID);
    void Check_For_Action(String sAction, String sValue, int i16jsonValue, int* ai16ResponseData);
    void Update_AC_Input_Values(uint8_t ui8DeviceIndex, int* ai16ResponseData);
};

#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_TWOWIRE)
extern ESPApplication EApp;
#endif
#endif