/*******************************************************************************
*File Name: ESPAlexa.cpp
*
* Version: 1.0
*
* Description:
* In this source code for alexa usage
*
*
* Owner:
* Yogesh M Iggalore
*
********************************************************************************
* Copyright (2020-21) , ESP M2M India Pvt Ltd
*******************************************************************************/

#include <ESPAlexaV2.h>
#include <WebSocketsClient.h>
#include <ArduinoJson.h> 
#include <StreamString.h>
#include <ESPUtils.h>
#include <FS.h>
#include <ESPFile.h>
#include <Application.h>
#include <ACControl.h>

uint8_t ui8IsConnected=0;
uint64_t heartbeatTimestamp = 0;
uint8_t ui8HallLightValue=0;
uint8_t ui8RoomLightValue=0;
uint8_t ui8KitchenLightValue=0;
uint8_t ui8ChangeValueFlag=0;

ESPAlexa::ESPAlexa(void):webSocket(),client(){

}

String Get_StringValue(uint8_t ui8Device, uint8_t ui8Value){
    unsigned char ui8dig1,ui8dig2,ui8dig3,sDig[3];
    unsigned char ui8Temp;
    String sStringValue;

    ui8Temp=ui8Value;
    ui8Temp=ui8Temp/10;
    ui8dig1=ui8Value%10;
    ui8dig2=ui8Temp%10;
    ui8dig3=ui8Temp/10;

    sDig[0]=ui8dig3;
    sDig[1]=ui8dig2;
    sDig[2]=ui8dig1;

    EUtils.println(sDig[0]);
    EUtils.println(sDig[1]);
    EUtils.println(sDig[2]);

    if(ui8Device == HALL_LIGHT_FLAG){
        sStringValue = "A" ;
        sStringValue = sStringValue + sDig[0];
        sStringValue = sStringValue + sDig[1];
        sStringValue = sStringValue + sDig[2];
        EUtils.println("HALL_LIGHT_FLAG");
        EUtils.println(sStringValue);
    }else if(ui8Device == ROOM_LIGHT_FLAG){
        sStringValue = "B" ;
        sStringValue = sStringValue + sDig[0];
        sStringValue = sStringValue + sDig[1];
        sStringValue = sStringValue + sDig[2];
        EUtils.println("ROOM_LIGHT_FLAG");
        EUtils.println(sStringValue);
    }else if(ui8Device == KITCHEN_LIGHT_FLAG){
        sStringValue = "C" ;
        sStringValue = sStringValue + sDig[0];
        sStringValue = sStringValue + sDig[1];
        sStringValue = sStringValue + sDig[2];
        EUtils.println("KITCHEN_LIGHT_FLAG");
        EUtils.println(sStringValue);
    }else{
        //do nothing
    }
    EUtils.println(sStringValue);
    return sStringValue;
}

void handleSerial1Dimmer(void){
    String sSerial1Value;

    if(ui8ChangeValueFlag){
        EUtils.println(ui8ChangeValueFlag);
        EUtils.println(ui8HallLightValue);
        EUtils.println(ui8RoomLightValue);
        EUtils.println(ui8KitchenLightValue);
        if(ui8ChangeValueFlag == HALL_LIGHT_FLAG){
            sSerial1Value = Get_StringValue(HALL_LIGHT_FLAG,ui8HallLightValue);
        }else if(ui8ChangeValueFlag == ROOM_LIGHT_FLAG){
            sSerial1Value = Get_StringValue(ROOM_LIGHT_FLAG,ui8RoomLightValue);
        }else if(ui8ChangeValueFlag == KITCHEN_LIGHT_FLAG){
            sSerial1Value = Get_StringValue(KITCHEN_LIGHT_FLAG,ui8KitchenLightValue);
        }else{
            // do nothing
        }
        ui8ChangeValueFlag = 0;
        if(sSerial1Value){
            Serial1.print(sSerial1Value);
        }
    }
}

void Action_Selector(uint8_t * payload){
    DynamicJsonDocument json(1024);
    deserializeJson(json, payload);

    String sDeviceId = json ["deviceId"];
    String sAction = json ["action"];
    String sValue = json["value"];
    int i16Value = json["value"];
    EUtils.print("Sinric data: ");
    EUtils.print(sAction);
    EUtils.print(" ");
    EUtils.print(sDeviceId);
    EUtils.print(" ");
    EUtils.println(sValue);
    if(sAction == "setPowerState"){
        if(sDeviceId == HALL_LIGHT_ID){
            ui8ChangeValueFlag = HALL_LIGHT_FLAG;
            if(sValue == DEVICE_ON){
                ui8HallLightValue = 100;
                EUtils.println("Turning on hall light");
            }else{
                ui8HallLightValue = 0;
                EUtils.println("Turning off hall light");
            }
        }else if(sDeviceId == ROOM_LIGHT_ID){
            ui8ChangeValueFlag = ROOM_LIGHT_FLAG;
            if(sValue == DEVICE_ON){
                ui8RoomLightValue = 100;
                EUtils.println("Turning on room light");
            }else{
                ui8RoomLightValue = 0;
                EUtils.println("Turning off room light");
            }
        }else if(sDeviceId == KITCHEN_LIGHT_ID){
            ui8ChangeValueFlag = KITCHEN_LIGHT_FLAG;
            if(sValue == DEVICE_ON){
                ui8KitchenLightValue = 100;
                EUtils.println("Turning on kitchen light");
            }else{
                ui8KitchenLightValue = 0;
                EUtils.println("Turning off kitchen light");
            }
        }else{
            EUtils.println("No device specified in code");
        }
    }else if(sAction == "SetBrightness"){
        if(sDeviceId == HALL_LIGHT_ID){
            ui8ChangeValueFlag = HALL_LIGHT_FLAG;
            ui8HallLightValue = i16Value;
            EUtils.println("setting hall light brightness");
        }else if(sDeviceId == ROOM_LIGHT_ID){
            ui8ChangeValueFlag = ROOM_LIGHT_FLAG;
            ui8RoomLightValue = i16Value;
            EUtils.println("setting room light brightness");
        }else if(sDeviceId == KITCHEN_LIGHT_ID){
            ui8ChangeValueFlag = KITCHEN_LIGHT_FLAG;
            ui8KitchenLightValue = i16Value;
            EUtils.println("setting kitchen light brightness");
        }else{
            EUtils.println("No device specified in code");
        }
    }else if(sAction == "AdjustBrightness"){
        if(sDeviceId == HALL_LIGHT_ID){
            if(i16Value < 0){
                if(ui8HallLightValue >= ALEXA_BRIGHTNESS_LEVEL){
                    ui8HallLightValue = ui8HallLightValue - ALEXA_BRIGHTNESS_LEVEL;
                    ui8ChangeValueFlag = HALL_LIGHT_FLAG;
                    EUtils.println("Adjusting brightness of hall light");
                }
            }else{
                if(ui8HallLightValue < 100){
                    ui8HallLightValue = ui8HallLightValue + ALEXA_BRIGHTNESS_LEVEL;
                    ui8ChangeValueFlag = HALL_LIGHT_FLAG;
                    EUtils.println("Adjusting brightness of hall light");
                }
            }
        }else if(sDeviceId == ROOM_LIGHT_ID){
            if(i16Value < 0){
                if(ui8RoomLightValue >= ALEXA_BRIGHTNESS_LEVEL){
                    ui8RoomLightValue = ui8RoomLightValue - ALEXA_BRIGHTNESS_LEVEL;
                    ui8ChangeValueFlag = ROOM_LIGHT_FLAG;
                    EUtils.println("Adjusting brightness of room light");
                }
            }else{
                if(ui8RoomLightValue < 100){
                    ui8RoomLightValue = ui8RoomLightValue + ALEXA_BRIGHTNESS_LEVEL;
                    ui8ChangeValueFlag = ROOM_LIGHT_FLAG;
                    EUtils.println("Adjusting brightness of room light");
                }
            }
        }else if(sDeviceId == KITCHEN_LIGHT_ID){
            if(i16Value < 0){
                if(ui8KitchenLightValue >= ALEXA_BRIGHTNESS_LEVEL){
                    ui8KitchenLightValue = ui8KitchenLightValue - ALEXA_BRIGHTNESS_LEVEL;
                    ui8ChangeValueFlag = KITCHEN_LIGHT_FLAG;
                    EUtils.println("Adjusting brightness of kitchen light");
                }
            }else{
                if(ui8KitchenLightValue < 100){
                    ui8KitchenLightValue = ui8KitchenLightValue + ALEXA_BRIGHTNESS_LEVEL;
                    ui8ChangeValueFlag = KITCHEN_LIGHT_FLAG;
                    EUtils.println("Adjusting brightness of kitchen light");
                }
            }
        }else{
            EUtils.println("No device specified in code");
        }
    }else{
        // do nothing
    }

    handleSerial1Dimmer();
}

void webSocketEvent(WStype_t type, uint8_t * payload, size_t length){
    switch(type) {
        case WStype_DISCONNECTED:
            ui8IsConnected = false;
            EUtils.println("[WSc] Webservice disconnected from sinric.com!\n");
            break;
        case WStype_CONNECTED:
            ui8IsConnected = true;
            EUtils.println("[WSc] Service connected to sinric.com at url: " );
            //EUtils.println(String(payload).);
            EUtils.println("Waiting for commands from sinric.com ...\n");
            break;
        case WStype_ERROR:
            EUtils.println("WStype_ERROR");
            break;
        case WStype_FRAGMENT_TEXT_START:
            EUtils.println("WStype_FRAGMENT_TEXT_START");
            break;
        case WStype_FRAGMENT_BIN_START:
            EUtils.println("WStype_FRAGMENT_BIN_START");
            break;
        case WStype_FRAGMENT:
            EUtils.println("WStype_FRAGMENT");
            break;
        case WStype_FRAGMENT_FIN:
            EUtils.println("WStype_FRAGMENT_FIN");
            break;
        case WStype_TEXT:
            //Action_Selector(payload);
            EUtils.println("Sinric Data received...");
            EApp.HandleAlexa(payload);
            break;
        case WStype_BIN:
            EUtils.print("[WSc] get binary length: ");
            EUtils.println(length);
            break;
    }
}

void ESPAlexa::Start(void){

    Update_All_Key_And_ID();

    // server address, port and URL
    webSocket.begin("iot.sinric.com", 80, "/");
    // event handler
    webSocket.onEvent(webSocketEvent);
    //webSocket.setAuthorization("apikey", MyApiKey);
    
    webSocket.setAuthorization("apikey", sAlexaAPIKey.c_str());
    
    // try again every 5000ms if connection has failed
    webSocket.setReconnectInterval(5000);
}

void ESPAlexa::handleRequest(void){
    webSocket.loop();
    if(ui8IsConnected) {
        uint64_t now = millis();
        // Send heartbeat in order to avoid disconnections during ISP resetting IPs over night. Thanks @MacSass
        if((now - heartbeatTimestamp) > HEARTBEAT_INTERVAL) {
            heartbeatTimestamp = now;
            webSocket.sendTXT("H");
            EUtils.println("sinric heartbeat");
        }
    }
}

void ESPAlexa::Update_Specified_File(uint8_t ui8ID){
    String sString;

    sString = EFile.Read_Data_From_File(sESPFiles[ui8ID]);
    EUtils.println(sString);

    if(ui8ID == ALEXA_TOKEN_INDEX){
        sAlexaAPIKey = sString;
    }else if(ui8ID == ALEXA_AC_1_ID_INDEX){
        sAlexaACInput1ID = sString;
    }else if(ui8ID == ALEXA_AC_2_ID_INDEX){
        sAlexaACInput2ID = sString;
    }else if(ui8ID == ALEXA_AC_3_ID_INDEX){
        sAlexaACInput3ID = sString;
    }else if(ui8ID == ALEXA_AC_4_ID_INDEX){
        sAlexaACInput4ID = sString;
    }else if(ui8ID == ALEXA_AC_1_NAME_INDEX){
        sAlexaACInput1Name = sString;
    }else if(ui8ID == ALEXA_AC_2_NAME_INDEX){
        sAlexaACInput2Name = sString;
    }else if(ui8ID == ALEXA_AC_3_NAME_INDEX){
        sAlexaACInput3Name = sString;
    }else if(ui8ID == ALEXA_AC_4_NAME_INDEX){
        sAlexaACInput4Name = sString;
    }else{
        EUtils.println("file error");
    }
}

void ESPAlexa::Update_All_Key_And_ID(void){
    
    sAlexaAPIKey = EFile.Read_Data_From_File(sESPFiles[ALEXA_TOKEN_INDEX]);
    EUtils.print("API Key:");
    EUtils.println(sAlexaAPIKey);
    

    sAlexaACInput1ID = EFile.Read_Data_From_File(sESPFiles[ALEXA_AC_1_ID_INDEX]);
    EUtils.print("ACin1:");    
    EUtils.println(sAlexaACInput1ID);


    sAlexaACInput2ID = EFile.Read_Data_From_File(sESPFiles[ALEXA_AC_2_ID_INDEX]);
    EUtils.print("ACin2:"); 
    EUtils.println(sAlexaACInput2ID);


    sAlexaACInput3ID = EFile.Read_Data_From_File(sESPFiles[ALEXA_AC_3_ID_INDEX]);
    EUtils.print("ACin3:"); 
    EUtils.println(sAlexaACInput3ID);


    sAlexaACInput4ID = EFile.Read_Data_From_File(sESPFiles[ALEXA_AC_4_ID_INDEX]);
    EUtils.print("ACin4:"); 
    EUtils.println(sAlexaACInput4ID);


    sAlexaACInput1Name = EFile.Read_Data_From_File(sESPFiles[ALEXA_AC_1_NAME_INDEX]);
    EUtils.print("ACName1:"); 
    EUtils.println(sAlexaACInput1Name);


    sAlexaACInput2Name = EFile.Read_Data_From_File(sESPFiles[ALEXA_AC_2_NAME_INDEX]);
    EUtils.print("ACName2:");
    EUtils.println(sAlexaACInput2Name);


    sAlexaACInput3Name = EFile.Read_Data_From_File(sESPFiles[ALEXA_AC_3_NAME_INDEX]);
    EUtils.print("ACName3:");
    EUtils.println(sAlexaACInput3Name);


    sAlexaACInput4Name = EFile.Read_Data_From_File(sESPFiles[ALEXA_AC_4_NAME_INDEX]);
    EUtils.print("ACName4:");
    EUtils.println(sAlexaACInput4Name);

}

String ESPAlexa::Get_Value_Of_File(uint8_t ui8Index){
    String sString;

    if(ui8Index == ALEXA_TOKEN_INDEX){
        sString= sAlexaAPIKey;
    }else if(ui8Index == ALEXA_AC_1_ID_INDEX){
        sString= sAlexaACInput1ID;
    }else if(ui8Index == ALEXA_AC_2_ID_INDEX){
        sString= sAlexaACInput2ID;
    }else if(ui8Index == ALEXA_AC_3_ID_INDEX){
        sString= sAlexaACInput3ID;
    }else if(ui8Index == ALEXA_AC_4_ID_INDEX){
        sString= sAlexaACInput4ID;
    }else if(ui8Index == ALEXA_AC_1_NAME_INDEX){
        sString= sAlexaACInput1Name;
    }else if(ui8Index == ALEXA_AC_2_NAME_INDEX){
        sString= sAlexaACInput2Name;
    }else if(ui8Index == ALEXA_AC_3_NAME_INDEX){
        sString= sAlexaACInput3Name;
    }else if(ui8Index == ALEXA_AC_4_NAME_INDEX){
        sString= sAlexaACInput4Name;
    }else{
        sString= "  ";
    }

    return sString;
}

// Preinstantiate Objects //////////////////////////////////////////////////////

#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_TWOWIRE)
ESPAlexa MAlexa;
#endif
